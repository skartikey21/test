var str = 'a';

var sortArr = str.split('').sort();

var object = {};

sortArr.forEach((ele,i) => {
    object[ele] = (object[ele] || 0) + 1;
})

var finalArray = []
for(key in object) {
    finalArray.push(key,object[key])
}

console.log(finalArray.join(''))