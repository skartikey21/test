// // var cluster = require('cluster');
// // var http = require('http');
// // var numCPUs = 4;

// // if (cluster.isMaster) {
// //     for (var i = 0; i < numCPUs; i++) {
// //         cluster.fork();
// //     }
// // } else {
// //     console.log(process.pid)
// //     // http.createServer(function(req, res) {
// //     //     res.writeHead(200);
// //     //     res.end('process ' + process.pid + ' says hello!');
// //     // }).listen(8000);
// // }

// // const cluster = require('cluster')
// // const os = require('os')
// // const express = require('express')

// // if (cluster.isMaster) {
// //     const cpuCount = os.cpus().length
// //     for (let i = 0; i < cpuCount; i++) {
// //         cluster.fork()
// //     }
// // }
// // else {
// //     const app = express()

// //     app.get('/', (req, res) => {
// //         for(var i=0; i<4; i++){
// //             res.json(i)
// //         }
// //     })

// //     const port = process.env.PORT || 3030

// //     app.listen(port)

// //     console.log('app is running on port', port)
// // }

// // cluster.on('exit', (worker) => {
// //     console.log('mayday! mayday! worker', worker.id, ' is no more!')
// //     cluster.fork()
// // })
var cluster = require('cluster');
var express = require('express');
var numCPUs = require('os').cpus().length;

console.log(numCPUs, 'op')
if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        // Create a worker
        cluster.fork();
    }
} else {
    // Workers share the TCP connection in this server
    var app = express();

    app.get('/', function (req, res) {
        setTimeout(function() {
            res.send('Testing'+ process.pid);
            cluster.worker.kill();
        },2000)
    });

    // All workers use this port
    app.listen(8080);
}
